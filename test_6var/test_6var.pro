QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_tst_6.cpp \
    main.cpp

SOURCES +=  ../func.cpp

HEADERS += \
    tst_func.h

HEADERS += ../func.h
